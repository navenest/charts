# Notes

Due to the shift in strimzi to use SPS (Strimzi Pod Sets), argocd views the PVCs are unknown and deletes them while the operator creates them, ending up in an endless loop. Notes related to the issue:

- https://github.com/strimzi/strimzi-kafka-operator/issues/7093
- https://github.com/argoproj/argo-cd/discussions/10096
- https://github.com/orgs/strimzi/discussions/7205

The solution put in place for myself was to prefer to use the deleteClaim option so when the cluster is deleted so are the PVCs (in turn resulting in the PVCs to be managed by the deploy and deleted with as per the [issue](https://github.com/orgs/strimzi/discussions/7205))